<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PsBoxType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idHive')
            ->add('idClient')
            ->add('serialNumber')
            ->add('version')
            ->add('note')
            ->add('active')
            ->add('dateAdd')
            ->add('dateUpd')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\PsBox'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'apibundle_psbox';
    }
}
