<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class PsHiveType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idClient')
            ->add('name')
            ->add('note')
            ->add('latitude')
            ->add('longitude')
            ->add('active')
            ->add('hiveType')
            ->add('beesType')
            ->add('material')
            ->add('support')
            ->add('state')
            ->add('harvest')
            ->add('notes')
            ->add('dateAdd')
            ->add('dateUpd')
            ->add('idHiveGroup')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\PsHive'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'apibundle_pshive';
    }
}
