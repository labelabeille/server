<?php

namespace ApiBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;

class PsCustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('idShopGroup')
            ->add('idShop')
            ->add('idGender')
            ->add('idDefaultGroup')
            ->add('idLang')
            ->add('idRisk')
            ->add('company')
            ->add('siret')
            ->add('ape')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('password')
            ->add('lastPasswdGen')
            ->add('birthday')
            ->add('newsletter')
            ->add('ipRegistrationNewsletter')
            ->add('newsletterDateAdd')
            ->add('optin')
            ->add('website')
            ->add('outstandingAllowAmount')
            ->add('showPublicPrices')
            ->add('maxPaymentDays')
            ->add('secureKey')
            ->add('note')
            ->add('active')
            ->add('isGuest')
            ->add('deleted')
            ->add('dateAdd')
            ->add('dateUpd')
            ->add('hivesNumber')
            ->add('hivesLostNumber')
            ->add('harvest')
            ->add('startYear')
            ->add('departament')
            ->add('registrationNumber')
            ->add('transhumance')
            ->add('structure')
            ->add('telephone')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\PsCustomer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'apibundle_pscustomer';
    }
}
