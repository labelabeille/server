<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use ApiBundle\Form\PsDefaultNoteCustomerType;
use ApiBundle\Entity\CommonInterface;


class PsDefaultNoteCustomerRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }    
    
    /**
     * List all PsDefaultNoteCustomers.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing psdefaultnotecustomers.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many psdefaultnotecustomers to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    /*public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();
        return $this->container->get('api.psdefaultnotecustomer.handler')->all();
    }

    /**
     * Get single psdefaultnotecustomer.
     *
     * @param int     $id      the psdefaultnotecustomer id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when psdefaultnotecustomer not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $psdefaultnotecustomer = $this->getOr404($id);
        return $psdefaultnotecustomer;
    }

    /**
     * Presents the form to use to create a new psdefaultnotecustomer.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        $this->forwardIfNotAuthenticated();
        return $this->createForm(new PsDefaultNoteCustomerType());
    }

    /**
     * Create an psdefaultnotecustomer from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsDefaultNoteCustomer.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->forwardIfNotAuthenticated();
        $params = $request->request->all();
        try {
	        $params['apibundle_psdefaultnotecustomer']['dateAdd'] = $this->now();
            $new = $this->container->get('api.psdefaultnotecustomer.handler')->post(
                $params
            );

            return new JsonResponse($new, Codes::HTTP_CREATED);
            $routeOptions = array(
                'id' => $new->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('api_psdefaultnotecustomer_get', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing psdefaultnotecustomer from the submitted data or create a new psdefaultnotecustomer at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsDefaultNoteCustomer.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psdefaultnotecustomer id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when psdefaultnotecustomer not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psdefaultnotecustomer = $this->getOr404($id);
		try {
			if (!($psdefaultnotecustomer = $this->container->get('api.psdefaultnotecustomer.handler')->get($id))) {
				$statusCode = Codes::HTTP_CREATED;
				$psdefaultnotecustomer = $this->container->get('api.psdefaultnotecustomer.handler')->post(
					$request->request->all()
				);
			} else {
				$statusCode = Codes::HTTP_NO_CONTENT;
				$psdefaultnotecustomer = $this->container->get('api.psdefaultnotecustomer.handler')->put(
					$psdefaultnotecustomer,
					$request->request->all()
				);
			}

			$routeOptions = array(
				'id' => $psdefaultnotecustomer->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_psdefaultnotecustomer_get', $routeOptions, $statusCode);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
    }

    /**
     * Update existing psdefaultnotecustomer from the submitted data or create a new psdefaultnotecustomer at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsDefaultNoteCustomer.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psdefaultnotecustomer id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when psdefaultnotecustomer not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psdefaultnotecustomer = $this->getOr404($id);
		try {
			$psdefaultnotecustomer = $this->container->get('api.psdefaultnotecustomer.handler')->patch(
				$this->getOr404($id),
				$request->request->all()
			);
			return $psdefaultnotecustomer;

			$routeOptions = array(
				'id' => $psdefaultnotecustomer->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_psdefaultnotecustomer_get', $routeOptions, Codes::HTTP_NO_CONTENT);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
    }

    /**
     * Fetch a PsDefaultNoteCustomer or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsDefaultNoteCustomer
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($psdefaultnotecustomer = $this->container->get('api.psdefaultnotecustomer.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $psdefaultnotecustomer;
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
	    return $this->container->get('api.psdefaultnotecustomer.handler')->delete($client);
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
    
    /**
     * List all PsCustomNoteCustomers for the selected hive.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="idHive", requirements="\d+", nullable=false, description="Identifier of the hive for which get the already added custom notes.")
     *
     * @param integer               $idHive      identifier of a hive
     *
     * @return array
     */
    public function getHiveAction($id) {
        $this->forwardIfNotAuthenticated();
        return $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsDefaultNoteCustomer')->findByIdHive($id);
    }
}
?>
