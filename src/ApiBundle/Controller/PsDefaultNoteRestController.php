<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;

use ApiBundle\Form\PsDefaultNoteType;
use ApiBundle\Entity\CommonInterface;


class PsDefaultNoteRestController extends FOSRestController
{
    /**
     * List all PsDefaultNotes.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing psdefaultnotes.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many psdefaultnotes to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();
        return $this->container->get('api.psdefaultnote.handler')->all();
    }

    /**
     * Get single psdefaultnote.
     *
     * @param int     $id      the psdefaultnote id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when psdefaultnote not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $psdefaultnote = $this->getOr404($id);
        return $psdefaultnote;
    }

    /**
     * Presents the form to use to create a new psdefaultnote.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        $this->forwardIfNotAuthenticated();
        return $this->createForm(new PsDefaultNoteType());
    }

    /**
     * Create an psdefaultnote from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsDefaultNote.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->forwardIfNotAuthenticated();
        $params = $request->request->all();
        try {
            $new = $this->container->get('api.psdefaultnote.handler')->post(
                $request->request->all()
            );

            return $new;
            $routeOptions = array(
                'id' => $new->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('api_psdefaultnote_get', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing psdefaultnote from the submitted data or create a new psdefaultnote at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsDefaultNote.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psdefaultnote id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when psdefaultnote not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psdefaultnote = $this->getOr404($id);
		try {
			if (!($psdefaultnote = $this->container->get('api.psdefaultnote.handler')->get($id))) {
				$statusCode = Codes::HTTP_CREATED;
				$psdefaultnote = $this->container->get('api.psdefaultnote.handler')->post(
					$request->request->all()
				);
			} else {
				$statusCode = Codes::HTTP_NO_CONTENT;
				$psdefaultnote = $this->container->get('api.psdefaultnote.handler')->put(
					$psdefaultnote,
					$request->request->all()
				);
			}

			$routeOptions = array(
				'id' => $psdefaultnote->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_psdefaultnote_get', $routeOptions, $statusCode);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
    }

    /**
     * Update existing psdefaultnote from the submitted data or create a new psdefaultnote at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsDefaultNote.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psdefaultnote id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when psdefaultnote not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psdefaultnote = $this->getOr404($id);
		try {
			$psdefaultnote = $this->container->get('api.psdefaultnote.handler')->patch(
				$this->getOr404($id),
				$request->request->all()
			);
			return $psdefaultnote;

			$routeOptions = array(
				'id' => $psdefaultnote->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_psdefaultnote_get', $routeOptions, Codes::HTTP_NO_CONTENT);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
    }

    /**
     * Fetch a PsDefaultNote or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsDefaultNote
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($psdefaultnote = $this->container->get('api.psdefaultnote.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $psdefaultnote;
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $psdefaultnote = $this->getOr404($id);
	    return $this->container->get('api.psdefaultnote.handler')->delete($psdefaultnote);
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
}
?>
