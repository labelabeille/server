<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;

use ApiBundle\Form\PsClientType;
use ApiBundle\Entity\CommonInterface;


class PsClientRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }    
    
    /**
     * List all PsClients.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing ppsclients.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many ppsclients to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();

        return $this->container->get('api.psclient.handler')->all();
    }

    /**
     * Get single psclient.
     *
     * @param int     $id      the psclient id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when ppsclient not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $psclient = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            return $psclient;
        }
        else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
        }
    }

    /**
     * Presents the form to use to create a new psclient.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        $this->forwardIfNotAuthenticated();
        return $this->createForm(new PsClientType());
    }

    /**
     * Create an psclient from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsClient.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->forwardIfNotAuthenticated();
        $params = $request->request->all();
		$params['apibundle_psclient']['idCustomer'] = $this->getUser()->getId();
	    $params['apibundle_pshive']['dateUpd'] = $this->now();
	    $params['apibundle_pshive']['dateAdd'] = $this->now();
        try {
            $new = $this->container->get('api.psclient.handler')->post(
                $params
            );

            return $new;
            $routeOptions = array(
                'id' => $new->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('api_psclient_get', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }
    
    /**
	 * Create an psclient from the submitted data.
	 *
	 * @Post()
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function createAction(Request $request) {
		$this->forwardIfNotAuthenticated();
		$client = $this->postAction($request);
		$idclient = $client->getId();
		$nomclient = $client->getName();
		$tel = $client->getTelephone();
		//$mail = $client->getMail();
		$url = "https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=createClient&idclient=$idclient";
		if($nomclient != null) {
		    $url.='&nomclient='.$nomclient;
		}
		if($tel != null) {
		    $url.="&tel=$tel";
		}
		/*if($mail != null) {
		    $url.="&mail=$mail";
		}*/

		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$resultat = curl_exec($ch);
		curl_close($ch);
		$resultat = json_decode(utf8_encode($resultat), true);

		return $resultat;*/
		return true;
	}

    /**
     * Update existing psclient from the submitted data or create a new psclient at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsClient.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psclient id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when psclient not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psclient = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            try {
		        $params = $request->request->all();
		        $params['apibundle_psclient']['dateUpd'] = $this->now();
                if (!($ppsclient = $this->container->get('api.psclient.handler')->get($id))) {
                    $statusCode = Codes::HTTP_CREATED;
                    $ppsclient = $this->container->get('api.psclient.handler')->post(
                        $params
                    );
                } else {
                    $statusCode = Codes::HTTP_NO_CONTENT;
                    $ppsclient = $this->container->get('api.psclient.handler')->put(
                        $ppsclient,
                        $params
                    );
                }

                $routeOptions = array(
                    'id' => $ppsclient->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_psclient_get', $routeOptions, $statusCode);

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
        }
        else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
    }

    /**
     * Update existing psclient from the submitted data or create a new psclient at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsClient.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psclient id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when ppsclient not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psclient = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            try {
		        $params = $request->request->all();
		        $params['apibundle_psclient']['dateUpd'] = $this->now();
                $ppsclient = $this->container->get('api.psclient.handler')->patch(
                    $this->getOr404($id),
                    $params
                );
                return $ppsclient;

                $routeOptions = array(
                    'id' => $ppsclient->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_psclient_get', $routeOptions, Codes::HTTP_NO_CONTENT);

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
        }
        else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
    }

    /**
     * Fetch a PsClient or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsClientInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($psclient = $this->container->get('api.psclient.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $psclient;
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $client = $this->getAction($id);
        $idCustomer = $this->getUser()->getId();
        if($client->getIdCustomer() == $idCustomer) {
	        /*$ch = curl_init();
		    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		    curl_setopt($ch, CURLOPT_URL, 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=deleteClient&idclient='.$id);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $resultat = curl_exec($ch);
		    curl_close($ch);*/
	        return $this->container->get('api.psclient.handler')->delete($client);
        }
        else {
            return "Vous essayez de supprimer un compte différent du vôtre";
        }
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
    
    /**
	 * Update an psclient from the submitted data.
	 * Patch()
	 *
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function updateAction(Request $request, $id) {
		$this->forwardIfNotAuthenticated();
		$client = $this->patchAction($request, $id);
        $idCustomer = $this->getUser()->getId();
		$params = $request->request->get('apibundle_psclient');
		if($client->getIdCustomer() == $idCustomer) {
		    $url = "https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=updateClient&idclient=$id";
		    if(isset($params['name'])) {
		        $url.='&nomclient='.$params['name'];
		    }
		    if(isset($params['telephone'])) {
		        $url.='&tel='.$params['telephone'];
		    }
		    /*if($params['mail'] != null) {
		        $url.='&mail='.$params['mail'];
		    }*/

		    /*$ch = curl_init();
		    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $resultat = curl_exec($ch);
		    $resultat = json_decode(utf8_encode($resultat), true);
		    curl_close($ch);
		    //return $resultat["data"][$idRuche];
		    return $resultat;*/
		    return true;
		}
		else {
		    return false;
		}
	}
}
?>
