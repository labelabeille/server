<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;

use ApiBundle\Form\PsHiveGroupType;
use ApiBundle\Entity\CommonInterface;

class PsHiveGroupRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }    
    
	/**
	 * List all PsHiveGroups.
	 *
	 *
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pshivegroups.")
	 * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pshivegroups to return.")
	 *
	 * @param Request               $request      the request object
	 * @param ParamFetcherInterface $paramFetcher param fetcher service
	 *
	 * @return array
	 */
	public function getAllAction()
	{/*
	    $offset = $paramFetcher->get('offset');
	    $offset = null == $offset ? 0 : $offset;
	    $limit = $paramFetcher->get('limit');
	  */
		return $this->container->get('api.pshivegroup.handler')->all(1000);
	}

	/**
	 * Get single pshivegroup.
	 *
	 * @param int     $id      the pshivegroup id
	 *
	 * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
	 * @return array
	 *
	 * @throws NotFoundHttpException when pshivegroup not exist
	 */
	public function getAction($id)
	{
        $this->forwardIfNotAuthenticated();
        $pshivegroup = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshivegroup->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
    		return $pshivegroup;
        }
        else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
        }
	}

	/**
	 * Presents the form to use to create a new pshivegroup.
	 *
	 * @Annotations\View(
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
	 * @return FormTypeInterface
	 */
	public function newAction()
	{
		return $this->createForm(new PsHiveGroupType());
	}

	/**
	 * Create an pshivegroup from the submitted data.
	 *
	 *
	 * @Annotations\View(
	 *  template = "ApiBundle::newPsHiveGroup.html.twig",
	 *  statusCode = Codes::HTTP_BAD_REQUEST,
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function postAction(Request $request)
	{
		try {
		    $params = $request->request->all();
		    $params['apibundle_pshivegroup']['idClient'] = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($this->getUser()->getId())[0]->getId();
	        $params['apibundle_pshivegroup']['dateUpd'] = $this->now();
	        $params['apibundle_pshivegroup']['dateAdd'] = $this->now();
			$new = $this->container->get('api.pshivegroup.handler')->post(
					$params
					);

			$routeOptions = array(
					'id' => $new->getIdHiveGroup(),
					'_format' => $request->get('_format')
					);

			return $this->routeRedirectView('api_pshivegroup_get', $routeOptions, Codes::HTTP_CREATED);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
	}

	/**
	 * Update existing pshivegroup from the submitted data or create a new pshivegroup at a specific location.
	 *
	 *
	 * @Annotations\View(
	 *  template = "ApiBundle::editPsHiveGroup.html.twig",
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"id": "\d+"})
	 *
	 * @param Request $request the request object
	 * @param int     $id      the pshivegroup id
	 *
	 * @return FormTypeInterface|View
	 *
	 * @throws NotFoundHttpException when pshivegroup not exist
	 */
	public function putAction(Request $request, $id)
	{
        $this->forwardIfNotAuthenticated();
        $pshivegroup = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshivegroup->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
		    try {
		        $params = $request->request->all();
	            $params['apibundle_pshivegroup']['dateUpd'] = $this->now();
			    if (!($pshivegroup = $this->container->get('api.pshivegroup.handler')->get($id))) {
				    $statusCode = Codes::HTTP_CREATED;
				    $pshivegroup = $this->container->get('api.pshivegroup.handler')->post(
						    $params
						    );
			    } else {
				    $statusCode = Codes::HTTP_NO_CONTENT;
				    $pshivegroup = $this->container->get('api.pshivegroup.handler')->put(
						    $pshivegroup,
						    $params
						    );
			    }

			    $routeOptions = array(
					    'id' => $pshivegroup->getIdHiveGroup(),
					    '_format' => $request->get('_format')
					    );

			    return $this->routeRedirectView('api_pshivegroup_get', $routeOptions, $statusCode);

		    } catch (InvalidFormException $exception) {

			    return $exception->getForm();
		    }
		}
		else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
	}

	/**
	 * Update existing pshivegroup from the submitted data or create a new pshivegroup at a specific location.
	 *
	 *
	 * @Annotations\View(
	 *  template = "ApiBundle::editPsHiveGroup.html.twig",
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"id": "\d+"})
	 *
	 * @param Request $request the request object
	 * @param int     $id      the pshivegroup id
	 *
	 * @return FormTypeInterface|View
	 *
	 * @throws NotFoundHttpException when pshivegroup not exist
	 */
	public function patchAction(Request $request, $id)
	{
        $this->forwardIfNotAuthenticated();
        $pshivegroup = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshivegroup->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
		    try {
		        $params = $request->request->all();
	            $params['apibundle_pshivegroup']['dateUpd'] = $this->now();
			    $pshivegroup = $this->container->get('api.pshivegroup.handler')->patch(
					    $this->getOr404($id),
					    $params
					    );

			    $routeOptions = array(
					    'id' => $pshivegroup->getIdHiveGroup(),
					    '_format' => $request->get('_format')
					    );

			    return $this->routeRedirectView('api_pshivegroup_get', $routeOptions, Codes::HTTP_NO_CONTENT);

		    } catch (InvalidFormException $exception) {

			    return $exception->getForm();
		    }
		}
		else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
	}

	/**
	 * Fetch a PsHiveGroup or throw an 404 Exception.
	 *
	 * @param mixed $id
	 *
	 * @return PsHiveGroupInterface
	 *
	 * @throws NotFoundHttpException
	 */
	protected function getOr404($id)
	{
		if (!($pshivegroup = $this->container->get('api.pshivegroup.handler')->get($id))) {
			throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
		}

		return $pshivegroup;
	}
	
	/**
	 * return the list of all the HiveGroups for the current customer
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function meAction() {
        $this->forwardIfNotAuthenticated();
	    $idCustomer = $this->getUser()->getId();
	    $idClient = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($idCustomer);
	    $t = [];
	    foreach($idClient as $element) {
	        $c = $element->getId();
	        $ruches = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsHiveGroup')->findByIdClient($c);
	        foreach($ruches as $element) {
	            $t[] = $element;
	        }
	    }
	    return $t;
    }
    
    /**
     * return the list of hives of the HiveGroup with number $id
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function getHivesAction($id) {
        $this->forwardIfNotAuthenticated();
        $pshivegroup = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshivegroup->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            $ruches = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsHive')->findByIdHiveGroup($id);
            $pshivegroup->ruches = $ruches;
            return $pshivegroup->get_json();
		}
		else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
		}
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $hivegroup = $this->getAction($id);
        $idCustomer = $this->getUser()->getId();
        if (!($psclient = $this->container->get('api.psclient.handler')->get($hivegroup->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            /* link the hives from the group to an other group (the first one in the list) */
            $hivegroups = $this->meAction();
            $hives = $this->getMeAction($id);
            foreach($hives as $hive) {
                $param['apibundle_pshive']['idHiveGroup'] = $hivegroups[0]->getId();
                $this->container->get('api.pshive.handler')->patch($hive, $params);
            }
            /* delete the hive group */
	        return $this->container->get('api.pshivegroup.handler')->delete($hivegroup);
        }
        else {
            return "La ruche que vous souhaitez supprimer ne vous appartient pas.";
        }
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
    
}
?>
