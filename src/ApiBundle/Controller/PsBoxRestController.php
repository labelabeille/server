<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;

use ApiBundle\Form\PsBoxType;
use ApiBundle\Entity\CommonInterface;


class PsBoxRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }   

    /**
     * List all PsBoxs.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing ppsboxs.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many ppsboxs to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();
        $offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        return $this->container->get('api.psbox.handler')->all($limit, $offset);
    }

    /**
     * Get single psbox.
     *
     * @param int     $id      the psbox id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when ppsbox not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $psbox = $this->getOr404($id);
        if (!($pshive = $this->container->get('api.pshive.handler')->get($psbox->getIdHive()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $pshive->getId()));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            return $psbox;
        }
        else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
        }
    }

    /**
     * Presents the form to use to create a new psbox.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        $this->forwardIfNotAuthenticated();
        return $this->createForm(new PsBoxType());
    }

    /**
     * Create an psbox from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsBox.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->forwardIfNotAuthenticated();
        $params = $request->request->all();
		$params['apibundle_psbox']['idClient'] = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($this->getUser()->getId())[0]->getId();
		// test if hive really exists
		$hive = $this->container->get('api.pshive.handler')->get($params['apibundle_psbox']['idHive']);
		$params['apibundle_psbox']['dateUpd'] = $this->now();
		$params['apibundle_psbox']['dateAdd'] = $this->now();
		if(!(!$hive) && $hive->getIdClient() == $params['apibundle_psbox']['idClient']) {
            try {
                $new = $this->container->get('api.psbox.handler')->post(
                    $params
                );
                return $new;
                $routeOptions = array(
                    'id' => $new->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_psbox_get', $routeOptions, Codes::HTTP_CREATED);
            } catch (InvalidFormException $exception) {
                return $exception->getForm();
            }
        }
        else {
            return "La ruche n'existe pas.";
        }
    }

    /**
     * Update existing psbox from the submitted data or create a new psbox at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsBox.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psbox id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when psbox not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psbox = $this->getOr404($id);
        if (!($pshive = $this->container->get('api.pshive.handler')->get($psbox->getIdHive()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $pshive->getId()));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            try {
		        $params = $request->request->all();
		        $params['apibundle_psbox']['dateUpd'] = $this->now();
                if (!($ppsbox = $this->container->get('api.psbox.handler')->get($id))) {
                    $statusCode = Codes::HTTP_CREATED;
                    $ppsbox = $this->container->get('api.psbox.handler')->post(
                        $params
                    );
                } else {
                    $statusCode = Codes::HTTP_NO_CONTENT;
                    $ppsbox = $this->container->get('api.psbox.handler')->put(
                        $ppsbox,
                        $params
                    );
                }

                $routeOptions = array(
                    'id' => $ppsbox->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_psbox_get', $routeOptions, $statusCode);

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
        }
        else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
    }

    /**
     * Update existing psbox from the submitted data or create a new psbox at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsBox.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the psbox id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when ppsbox not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $psbox = $this->getOr404($id);
        if (!($pshive = $this->container->get('api.pshive.handler')->get($psbox->getIdHive()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $pshive->getId()));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            try {
		        $params = $request->request->all();
		        $params['apibundle_psbox']['dateUpd'] = $this->now();
                $ppsbox = $this->container->get('api.psbox.handler')->patch(
                    $this->getOr404($id),
                    $params
                );
                return $ppsbox;

                $routeOptions = array(
                    'id' => $ppsbox->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_psbox_get', $routeOptions, Codes::HTTP_NO_CONTENT);

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
        }
        else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
    }

    /**
     * Fetch a PsBox or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsBoxInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($psbox = $this->container->get('api.psbox.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $psbox;
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $psbox = $this->getOr404($id);
        if (!($pshive = $this->container->get('api.pshive.handler')->get($psbox->getIdHive()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $pshive->getId()));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
	        /*$ch = curl_init();
		    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		    curl_setopt($ch, CURLOPT_URL, 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=deleteLogger&idlogger='.$id);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    $resultat = curl_exec($ch);
		    curl_close($ch);*/
	        return $this->container->get('api.psbox.handler')->delete($psbox);
        }
        else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
        }
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }

	/**
     * POST Route annotation.
     * @Post()
	 * Create an box from the submitted data.
	 *
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function createAction(Request $request) {
		$this->forwardIfNotAuthenticated();
		$box = $this->postAction($request);
		/*$idruche = $box->getIdHive();
		$idlogger = $box->getId();
		$numlogger = $box->getSerialNumber();
		$url = 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=createLogger&idlogger='.$idlogger.'&numlogger='.$numlogger.'&idruche='.$idruche;
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$resultat = curl_exec($ch);
		curl_close($ch);
		$resultat = json_decode(utf8_encode($resultat), true);
		return $resultat;*/
		return $box;
	}

	/**
	 * Update an box from the submitted data.
	 *
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function updateAction(Request $request, $id) {
		$this->forwardIfNotAuthenticated();
		$params = $request->request->all();
		$numlogger = $params['apibundle_psbox']['serialNumber'];
		$url = 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=updateLogger&idlogger='.$id.'&numlogger='.$numlogger; 
		if(isset($params['apibundle_psbox']['idHive'])) {
		    $url .= '&idruche='.$params['apibundle_psbox']['idHive'];
		}
		/*$ch = curl_init();
	    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$resultat = curl_exec($ch);
		curl_close($ch);
		$resultat = json_decode(utf8_encode($resultat), true);*/
		return $this->patchAction($request, $id);
	}
}
?>
