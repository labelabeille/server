<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use ApiBundle\Form\PsCustomNoteType;
use ApiBundle\Entity\CommonInterface;


class PsCustomNoteRestController extends FOSRestController
{
    /**
     * List all PsCustomNotes for the current user.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pscustomnotes.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pscustomnotes to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();
        $idCustomer = $this->getUser()->getId();
        return $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsCustomNote')->findByIdCustomer($idCustomer);
    }

    /**
     * Get single pscustomnote.
     *
     * @param int     $id      the pscustomnote id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when pscustomnote not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $pscustomnote = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($pscustomnote->getIdCustomer() == $idCustomer) {
            return $pscustomnote;
        }
        else {
            return "Vous essayez de récupérer une note qui ne vous appartient pas.";
        }
    }

    /**
     * Presents the form to use to create a new pscustomnote.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        $this->forwardIfNotAuthenticated();
        return $this->createForm(new PsCustomNoteType());
    }

    /**
     * Create an pscustomnote from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsCustomNote.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->forwardIfNotAuthenticated();
        $params = $request->request->all();
        $params = $request->request->all();
		$params['apibundle_pscustomnote']['idCustomer'] = $this->getUser()->getId();
        try {
            $new = $this->container->get('api.pscustomnote.handler')->post(
                $params
            );

            return new JsonResponse($new, Codes::HTTP_CREATED);
            $routeOptions = array(
                'id' => $new->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('api_pscustomnote_get', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing pscustomnote from the submitted data or create a new pscustomnote at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsCustomNote.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the pscustomnote id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when pscustomnote not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $pscustomnote = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($pscustomnote->getIdCustomer() == $idCustomer) {
		    try {
			    if (!($pscustomnote = $this->container->get('api.pscustomnote.handler')->get($id))) {
				    $statusCode = Codes::HTTP_CREATED;
				    $pscustomnote = $this->container->get('api.pscustomnote.handler')->post(
					    $request->request->all()
				    );
			    } else {
				    $statusCode = Codes::HTTP_NO_CONTENT;
				    $pscustomnote = $this->container->get('api.pscustomnote.handler')->put(
					    $pscustomnote,
					    $request->request->all()
				    );
			    }

			    $routeOptions = array(
				    'id' => $pscustomnote->getId(),
				    '_format' => $request->get('_format')
			    );

			    return $this->routeRedirectView('api_pscustomnote_get', $routeOptions, $statusCode);

		    } catch (InvalidFormException $exception) {

			    return $exception->getForm();
		    }
        }
        else {
            return "Vous essayez de modifier une note qui ne vous appartient pas.";
        }
    }

    /**
     * Update existing pscustomnote from the submitted data or create a new pscustomnote at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsCustomNote.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the pscustomnote id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when pscustomnote not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $pscustomnote = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($pscustomnote->getIdCustomer() == $idCustomer) {
		    try {
			    $pscustomnote = $this->container->get('api.pscustomnote.handler')->patch(
				    $this->getOr404($id),
				    $request->request->all()
			    );
			    return $pscustomnote;

			    $routeOptions = array(
				    'id' => $pscustomnote->getId(),
				    '_format' => $request->get('_format')
			    );

			    return $this->routeRedirectView('api_pscustomnote_get', $routeOptions, Codes::HTTP_NO_CONTENT);

		    } catch (InvalidFormException $exception) {

			    return $exception->getForm();
		    }
        }
        else {
            return "Vous essayez de modifier une note qui ne vous appartient pas.";
        }
    }

    /**
     * Fetch a PsCustomNote or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsCustomNote
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($pscustomnote = $this->container->get('api.pscustomnote.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $pscustomnote;
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $pscustomnote = $this->getOr404($id);
        $idCustomer = $this->getUser()->getId();
        if($pscustomnote->getIdCustomer() == $idCustomer) {
            return $this->container->get('api.pscustomnote.handler')->delete($pscustomnote);
        }
        else {
            return "Vous essayez de supprimer une note qui ne vous appartient pas.";
        }
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
}
?>
