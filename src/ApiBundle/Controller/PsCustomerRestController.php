<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;

use ApiBundle\Form\PsCustomerType;
use ApiBundle\Entity\CommonInterface;
use ApiBundle\Entity\PsCustomer;


class PsCustomerRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }    
    
    /**
     * List all PsCustomers.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing ppscustomers.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many ppscustomers to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();
        $offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        return $this->container->get('api.pscustomer.handler')->all($limit, $offset);
    }

    /**
     * Get single pscustomer.
     *
     * @param int     $id      the pscustomer id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when ppscustomer not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $idCustomer = $this->getUser()->getId();
        if($id == $idCustomer) {
            $ppscustomer = $this->getOr404($id);
            return $ppscustomer;
        }
        else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
        }
    }

    /**
     * Presents the form to use to create a new pscustomer.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        return $this->createForm(new PsCustomerType());
    }

    /**
     * Create an pscustomer from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsCustomer.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        try {
            $user = new PsCustomer();
            $encoder = $this->container->get('security.password_encoder');
            $p = $request->request->get('apibundle_pscustomer');
            $encoded = $encoder->encodePassword($user, $p['password']);
            $p['password'] = $encoded;
	        $p['dateAdd'] = $this->now();
	        $p['lastPasswdGen'] = $this->now();
	        $p['dateUpd'] = $this->now();
            $request->request->set('apibundle_pscustomer', $p);
            $new = $this->container->get('api.pscustomer.handler')->post(
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $new->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('api_pscustomer_get', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing pscustomer from the submitted data or create a new pscustomer at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsCustomer.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the pscustomer id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when pscustomer not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $idCustomer = $this->getUser()->getId();
        if($id == $idCustomer) {
            try {
                if (!($ppscustomer = $this->container->get('api.pscustomer.handler')->get($id))) {
                    $statusCode = Codes::HTTP_CREATED;
                    $ppscustomer = $this->container->get('api.pscustomer.handler')->post(
                        $request->request->all()
                    );
                } else {
                    $statusCode = Codes::HTTP_NO_CONTENT;
                    $ppscustomer = $this->container->get('api.pscustomer.handler')->put(
                        $ppscustomer,
                        $request->request->all()
                    );
                }

                $routeOptions = array(
                    'id' => $ppscustomer->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_pscustomer_get', $routeOptions, $statusCode);

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
        }
        else {
            return "Vous essayez de modifier un compte différent du vôtre";
        } 
    }

    /**
     * Update existing pscustomer from the submitted data or create a new pscustomer at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsCustomer.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the pscustomer id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when ppscustomer not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $idCustomer = $this->getUser()->getId();
        if($id == $idCustomer) {
            try {
                $user = new PsCustomer();
                $encoder = $this->container->get('security.password_encoder');
                $p = $request->request->get('apibundle_pscustomer');
	            $p['dateUpd'] = $this->now();
                if(isset($p['password'])) {
                    $encoded = $encoder->encodePassword($user, $p['password']);
                    $p['password'] = $encoded;
	                $p['lastPasswdGen'] = $this->now();
                    $request->request->set('apibundle_pscustomer', $p);
                }
                $ppscustomer = $this->container->get('api.pscustomer.handler')->patch(
                    $this->getOr404($id),
                    $request->request->all()
                );
                return $ppscustomer;

                $routeOptions = array(
                    'id' => $ppscustomer->getId(),
                    '_format' => $request->get('_format')
                );

                return $this->routeRedirectView('api_pscustomer_get', $routeOptions, Codes::HTTP_NO_CONTENT);

            } catch (InvalidFormException $exception) {

                return $exception->getForm();
            }
        }
        else {
            return "Vous essayez de modifier un compte différent du vôtre";
        }
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $customer = $this->getAction($id);
        $idCustomer = $this->getUser()->getId();
        if($customer->getId() == $idCustomer) {
	        return $this->container->get('api.pscustomer.handler')->delete($customer);
        }
        else {
            return "Vous essayez de supprimer un compte différent du vôtre";
        }
    }

    /**
     * Fetch a PsCustomer or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsCustomerInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($pscustomer = $this->container->get('api.pscustomer.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $pscustomer;
    }

    /**
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function meAction(){
        $this->forwardIfNotAuthenticated();
        return $this->getUser();
        //return $this->getDoctrine()->getRepository('UserBundle:Auteurs')->findAll();
    }

    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }

    /**
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function meHivesAction() {
        $this->forwardIfNotAuthenticated();
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
        curl_setopt($ch, CURLOPT_URL, 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=getListeRuche');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $resultat = json_decode(curl_exec($ch), true);
	    curl_close($ch);*/
	    $idCustomer = $this->getUser()->getId();
	    $idClient = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($idCustomer);
	    $t = [];
	    foreach($idClient as $element) {
	        $c = $element->getId();
	        $ruches = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsHive')->findByIdClient($c);
	        foreach($ruches as $element) {
	            $t[] = $element;
	        }
	    }
	    return $t;
	    /*foreach($resultat["data"] as $cle=>$element) {
	        if($element["idClient"] != $idClient) {
	            unset($resultat["data"][$cle]);
	        }
	    }
	    return $resultat;*/
    }

    /**
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function meHivegroupsAction() {
        $this->forwardIfNotAuthenticated();
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
        curl_setopt($ch, CURLOPT_URL, 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=getListeRuche');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $resultat = json_decode(curl_exec($ch), true);
	    curl_close($ch);*/
	    $idCustomer = $this->getUser()->getId();
	    $idClient = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($idCustomer);
	    $t = [];
	    foreach($idClient as $element) {
	        $c = $element->getId();
	        $hivegroups = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsHiveGroup')->findByIdClient($c);
	        foreach($hivegroups as $element) {
	            $t[] = $element;
	        }
	    }
	    return $t;
	    /*foreach($resultat["data"] as $cle=>$element) {
	        if($element["idClient"] != $idClient) {
	            unset($resultat["data"][$cle]);
	        }
	    }
	    return $resultat;*/
    }

    /**
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function getHiveMeAction($idRuche) {
        $this->forwardIfNotAuthenticated();
        $f = 'ACCELERO_X,ACCELERO_Y,ACCELERO_Z,BAT,CODE_ERROR,DEF.DEF_BATTERIE_MIN,DEF.DEF_BATTERIE_MOY,DEF.DEF_COM,'.
        'DEF.DEF_GEO,DEF.DEF_HUM_MAX,DEF.DEF_HUM_MIN,DEF.DEF_MASSE,DEF.DEF_ORI,DEF.DEF_POIDS_TARE,DEF.DEF_TEMP_MAX,DEF.DEF_TEMP_MIN,DEF.DEF_VOL,'.
        'HORODATE,HUM,LAT,LNG,LUM,MAGNETO_X,MAGNETO_Y,MAGNETO_Z,MASSE,MODE,ORI,PARAM.COMMENTAIRE,PARAM.DASHBOARD_LARGE,PARAM.DASHBOARD_MIDDLE,'.
        'PARAM.DASHBOARD_SMALL,PARAM.FORFAIT,PARAM.GPS_DAYS,PARAM.IDENTIFIANT,PARAM.LIEN_EXTERNE,PARAM.MAINTENANCE,PARAM.NB_ABEILLE,PARAM.NB_HAUSSE,'.
        'PARAM.POIDS_ESSAIM,PARAM.POIDS_RECOLTE,PARAM.POIDS_RUCHE_VIERGE,PARAM.PROD_MIEL_HAUSSE,PARAM.PROD_MIEL_RUCHE,'.
        'PARAM.SEUIL_ACC_X_MAX,VOL,PARAM.SEUIL_ACC_X_MIN,PARAM.SEUIL_ACC_Y_MAX,PARAM.SEUIL_ACC_Y_MIN,PARAM.SEUIL_ACC_Z_MAX,'.
        'PARAM.SEUIL_ACC_Z_MIN,PARAM.SEUIL_BAISSE_POIDS,PARAM.SEUIL_BAISSE_POIDS_,DUREE,PARAM.SEUIL_HUMIDITE_MAX,PARAM.SEUIL_HUMIDITE_MIN,'.
        'PARAM.SEUIL_TEMP_MAX,PARAM.SEUIL_TEMP_MIN,PARAM.TITRE,PARAM.UPGRADE,PARAM.WAKEUP,PRESSION,SIGNAL_GPS,SIGNAL_GSM,SIGNAL_SIGFOX,TMP,VOL';
        $ch = curl_init();
        $url = 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=getInfoRuche&idRuche='.$idRuche.'&field='.$f;
        curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
        curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$resultat = curl_exec($ch);
		$resultat = json_decode(utf8_encode($resultat), true);
		curl_close($ch);
	    $idCustomer = $this->getUser()->getId();
	    $idClient = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($idCustomer);
	    $idsClient = [];
	    foreach($idClient as $element) {
	        $idsClient[] = $element->getId();
	    }
		if(!in_array($resultat["data"][$idRuche]["idclient"], $idsClient)) {
		    return "La ruche dont vous souhaitez voir les données ne vous appartient pas.";
		}
		return $resultat["data"][$idRuche];
    }
}
?>
