<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;

use ApiBundle\Form\PsHiveType;
use ApiBundle\Entity\CommonInterface;

class PsHiveRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }    
    
    
	/**
	 * List all PsHives.
	 *
	 *
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing ppshives.")
	 * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many ppshives to return.")
	 *
	 * @param Request               $request      the request object
	 * @param ParamFetcherInterface $paramFetcher param fetcher service
	 *
	 * @return array
	 */
	public function getAllAction()
	{/*
	    $offset = $paramFetcher->get('offset');
	    $offset = null == $offset ? 0 : $offset;
	    $limit = $paramFetcher->get('limit');
	  */
		return $this->container->get('api.pshive.handler')->all(1000);
	}

	/**
	 * Get single pshive.
	 *
	 * @param int     $id      the pshive id
	 *
	 * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
	 * @return array
	 *
	 * @throws NotFoundHttpException when ppshive not exist
	 */
	public function getAction($id)
	{
        $this->forwardIfNotAuthenticated();
        $pshive = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
    		return $pshive;
        }
        else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
        }
	}

	/**
	 * Presents the form to use to create a new pshive.
	 *
	 * @Annotations\View(
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
	 * @return FormTypeInterface
	 */
	public function newAction()
	{
		return $this->createForm(new PsHiveType());
	}

	/**
	 * Create an pshive from the submitted data.
	 *
	 *
	 * @Annotations\View(
	 *  template = "ApiBundle::newPsHive.html.twig",
	 *  statusCode = Codes::HTTP_BAD_REQUEST,
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function postAction(Request $request)
	{
		try {
		    $params = $request->request->all();
		    $params['apibundle_pshive']['idClient'] = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($this->getUser()->getId())[0]->getId();
		    $params['apibundle_pshive']['idHiveGroup'] = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsHiveGroup')->findByIdClient($params['apibundle_pshive']['idClient'])[0]->getId();
		    
		    $params['apibundle_pshive']['dateUpd'] = $this->now();
		    $params['apibundle_pshive']['dateAdd'] = $this->now();
			$new = $this->container->get('api.pshive.handler')->post(
					$params
					);
			return $new;

			$routeOptions = array(
					'id' => $new->getIdHive(),
					'_format' => $request->get('_format')
					);

			return $this->routeRedirectView('api_pshive_get', $routeOptions, Codes::HTTP_CREATED);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
	}

	/**
	 * Update existing pshive from the submitted data or create a new pshive at a specific location.
	 *
	 *
	 * @Annotations\View(
	 *  template = "ApiBundle::editPsHive.html.twig",
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"id": "\d+"})
	 *
	 * @param Request $request the request object
	 * @param int     $id      the pshive id
	 *
	 * @return FormTypeInterface|View
	 *
	 * @throws NotFoundHttpException when pshive not exist
	 */
	public function putAction(Request $request, $id)
	{
        $this->forwardIfNotAuthenticated();
        $pshive = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
		    try {
		        $params = $request->request->all();
		        $params['apibundle_pshive']['dateUpd'] = $this->now();
			    if (!($ppshive = $this->container->get('api.pshive.handler')->get($id))) {
				    $statusCode = Codes::HTTP_CREATED;
				    $ppshive = $this->container->get('api.pshive.handler')->post(
						    $params
						    );
			    } else {
				    $statusCode = Codes::HTTP_NO_CONTENT;
				    $pshive = $this->container->get('api.pshive.handler')->put(
						    $pshive,
						    $params
						    );
			    }

			    $routeOptions = array(
					    'id' => $ppshive->getIdHive(),
					    '_format' => $request->get('_format')
					    );

			    return $this->routeRedirectView('api_pshive_get', $routeOptions, $statusCode);

		    } catch (InvalidFormException $exception) {

			    return $exception->getForm();
		    }
		}
		else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
	}

	/**
	 * Update existing pshive from the submitted data or create a new pshive at a specific location.
	 *
	 *
	 * @Annotations\View(
	 *  template = "ApiBundle::editPsHive.html.twig",
	 *  templateVar = "form"
	 * )
	 * @Route(requirements={"id": "\d+"})
	 *
	 * @param Request $request the request object
	 * @param int     $id      the pshive id
	 *
	 * @return FormTypeInterface|View
	 *
	 * @throws NotFoundHttpException when ppshive not exist
	 */
	public function patchAction(Request $request, $id)
	{
        $this->forwardIfNotAuthenticated();
        $pshive = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
		    try {
		        $params = $request->request->all();
		        $params['apibundle_pshive']['dateUpd'] = $this->now();
			    $pshive = $this->container->get('api.pshive.handler')->patch(
					    $this->getOr404($id),
					    $params
					    );
			    return $pshive;

			    $routeOptions = array(
					    'id' => $ppshive->getIdHive(),
					    '_format' => $request->get('_format')
					    );

			    return $this->routeRedirectView('api_pshive_get', $routeOptions, Codes::HTTP_NO_CONTENT);

		    } catch (InvalidFormException $exception) {

			    return $exception->getForm();
		    }
		}
		else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
        }
	}

	/**
	 * Fetch a PsHive or throw an 404 Exception.
	 *
	 * @param mixed $id
	 *
	 * @return PsHiveInterface
	 *
	 * @throws NotFoundHttpException
	 */
	protected function getOr404($id)
	{
		if (!($pshive = $this->container->get('api.pshive.handler')->get($id))) {
			throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
		}

		return $pshive;
	}

	/**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
	public function getHivesCoordinatesAction() {
		$hives = $this->getAllAction();
		foreach($hives as $cle=>$hive) {
			$hives2[] = $hive->getCoordinates();
		}
		return $hives2;
	}
	
	/**
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function meAction() {
        $this->forwardIfNotAuthenticated();
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
        curl_setopt($ch, CURLOPT_URL, 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=getListeRuche');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    $resultat = json_decode(curl_exec($ch), true);
	    curl_close($ch);*/
	    $idCustomer = $this->getUser()->getId();
	    $idClient = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($idCustomer);
	    $t = [];
	    foreach($idClient as $element) {
	        $c = $element->getId();
	        $ruches = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsHive')->findByIdClient($c);
	        foreach($ruches as $element) {
	            $t[] = $element;
	        }
	    }
	    return $t;
	    /*foreach($resultat["data"] as $cle=>$element) {
	        if($element["idClient"] != $idClient) {
	            unset($resultat["data"][$cle]);
	        }
	    }
	    return $resultat;*/
    }
    
    /**
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     */
    public function getMeAction($idRuche) {
        $this->forwardIfNotAuthenticated();
        $pshive = $this->getOr404($idRuche);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$idRuche));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
            $f = 'ACCELERO_X,ACCELERO_Y,ACCELERO_Z,BAT,CODE_ERROR,DEF.DEF_BATTERIE_MIN,DEF.DEF_BATTERIE_MOY,DEF.DEF_COM,'.
            'DEF.DEF_GEO,DEF.DEF_HUM_MAX,DEF.DEF_HUM_MIN,DEF.DEF_MASSE,DEF.DEF_ORI,DEF.DEF_POIDS_TARE,DEF.DEF_TEMP_MAX,DEF.DEF_TEMP_MIN,DEF.DEF_VOL,'.
            'HORODATE,HUM,LAT,LNG,LUM,MAGNETO_X,MAGNETO_Y,MAGNETO_Z,MASSE,MODE,ORI,PARAM.COMMENTAIRE,PARAM.DASHBOARD_LARGE,PARAM.DASHBOARD_MIDDLE,'.
            'PARAM.DASHBOARD_SMALL,PARAM.FORFAIT,PARAM.GPS_DAYS,PARAM.IDENTIFIANT,PARAM.LIEN_EXTERNE,PARAM.MAINTENANCE,PARAM.NB_ABEILLE,PARAM.NB_HAUSSE,'.
            'PARAM.POIDS_ESSAIM,PARAM.POIDS_RECOLTE,PARAM.POIDS_RUCHE_VIERGE,PARAM.PROD_MIEL_HAUSSE,PARAM.PROD_MIEL_RUCHE,'.
            'PARAM.SEUIL_ACC_X_MAX,VOL,PARAM.SEUIL_ACC_X_MIN,PARAM.SEUIL_ACC_Y_MAX,PARAM.SEUIL_ACC_Y_MIN,PARAM.SEUIL_ACC_Z_MAX,'.
            'PARAM.SEUIL_ACC_Z_MIN,PARAM.SEUIL_BAISSE_POIDS,PARAM.SEUIL_BAISSE_POIDS_,DUREE,PARAM.SEUIL_HUMIDITE_MAX,PARAM.SEUIL_HUMIDITE_MIN,'.
            'PARAM.SEUIL_TEMP_MAX,PARAM.SEUIL_TEMP_MIN,PARAM.TITRE,PARAM.UPGRADE,PARAM.WAKEUP,PRESSION,SIGNAL_GPS,SIGNAL_GSM,SIGNAL_SIGFOX,TMP,VOL';
            $ch = curl_init();
            $url = 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=getInfoRuche&idRuche='.$idRuche.'&field='.$f;
            curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
            curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $resultat = curl_exec($ch);
		    $resultat = json_decode(utf8_encode($resultat), true);
		    curl_close($ch);
		    return $resultat["data"][$idRuche];
		}
		else {
            return "Vous essayez de récupérer les données d'un compte différent du vôtre";
		}
    }
	
	/**
	 * Create an pshive from the submitted data.
	 * @Post()
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function createAction(Request $request) {
		$this->forwardIfNotAuthenticated();
		$hive = $this->postAction($request);
		/* format params */
		$s = json_encode($request->request->get('params'));
		$idruche = $hive->getIdHive();
		//$idlogger = $hive->getIdBox(); // optionnel
		$idclient = $hive->getIdClient();
		$nomruche = $hive->getName();
		$url = 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=createRuche&idruche='.$idruche.'&idclient='.$idclient;
		if(isset($idlogger) && ($idlogger != null)) {
		    $url.='&idlogger='.$idlogger;
		}
		if($nomruche != null) {
		    $url.='&nomruche='.$nomruche;
		}
		if($s != null) {
		    $url.='&params='.$s;
		}
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$resultat = curl_exec($ch);
		$resultat = json_decode(utf8_encode($resultat), true);
		curl_close($ch);
		return $resultat['success'];*/
		return $hive;
	}

    /**
	 * Update an pshive from the submitted data.
	 *
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 *
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function updateAction(Request $request, $id) {
		$this->forwardIfNotAuthenticated();
        $pshive = $this->getOr404($id);
        if (!($psclient = $this->container->get('api.psclient.handler')->get($pshive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        $idCustomer = $this->getUser()->getId();
        if($psclient->getIdCustomer() == $idCustomer) {
		    $params = $request->request->get('apibundle_pshive');
		    $idclient = $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsClient')->findByIdCustomer($idCustomer)[0]->getId();
		    $url = 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=updateRuche&idruche='.$id;
		    if($idclient != null) {
		        $url.='&idclient='.$idclient;
		    }
		    if(isset($params['idBox'])) {
		        $idlogger = $params['idBox'];// optionnel ?
		        $url.='&idlogger='.$idlogger;
		    }
		    if(isset($params['name'])) {
		        $nomruche = $params['name'];
		        $url.='&nomruche='.$nomruche;
		    }
		    if(null !== $params = $request->request->get('params')) {
		        $s = '';
		        foreach($params as $cle => $element) {
		            $s .= '"'.$cle.'":"'.$element.'",';
		        }
		        $url.='&params={'.$s.'}';
		    }
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $resultat = curl_exec($ch);
		    curl_close($ch);
    		$resultat = json_decode(utf8_encode($resultat), true);
		    return $this->patchAction($request, $id);
		}
		else {
            return "Vous essayez de modifier les données d'un compte différent du vôtre";
		}
	}

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
        $hive = $this->getAction($id);
        $idCustomer = $this->getUser()->getId();
        if (!($psclient = $this->container->get('api.psclient.handler')->get($hive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        if($psclient->getIdCustomer() == $idCustomer) {
	        $ch = curl_init();
		    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		    curl_setopt($ch, CURLOPT_URL, 'https://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=deleteRuche&idruche='.$id);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $resultat = curl_exec($ch);
		    curl_close($ch);
	        return $this->container->get('api.pshive.handler')->delete($hive);
        }
        else {
            return "La ruche que vous souhaitez supprimer ne vous appartient pas.";
        }
    }
    
    /**
     * @Get()
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
	public function logAction(Request $request, $id) {
	    $this->forwardIfNotAuthenticated();
        $hive = $this->getAction($id);
        $idCustomer = $this->getUser()->getId();
        if (!($psclient = $this->container->get('api.psclient.handler')->get($hive->getIdClient()))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }
        if($psclient->getIdCustomer() == $idCustomer) {
            $params = $request->query->all();
            $s="";
		    foreach($params as $cle=>$element) {
			    $s=$s.$element.',';
		    }
	        $ch = curl_init();
		    curl_setopt($ch, CURLOPT_PROXY, '129.104.247.2:8080');
		    curl_setopt($ch, CURLOPT_URL, 'http://neotool.label-abeille.biz/kiwik/api?user=kiwik&pass=kiwik&method=getHistoRuche&idRuche='.$id.'&field='.$s.'&horodateDebut=01/01/2016%2000:00:00&horodateFin=01/04/2016%2000:00:00');
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    $resultat = curl_exec($ch);
		    curl_close($ch);
    		$resultat = json_decode(utf8_encode($resultat), true);
	        return $resultat;
        }
        else {
            return "La ruche que vous souhaitez voir l'historique ne vous appartient pas.";
        }
	}
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
}
?>
