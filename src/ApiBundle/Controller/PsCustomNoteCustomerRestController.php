<?php 

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

use ApiBundle\Form\PsCustomNoteCustomerType;
use ApiBundle\Entity\CommonInterface;


class PsCustomNoteCustomerRestController extends FOSRestController
{

    public function now() {
        $test = getdate();
		$date['date']['day'] = $test['mday'];
	    $date['date']['month'] = $test['mon'];
	    $date['date']['year'] = $test['year'];
	    $date['time']['minute'] = $test['minutes'];
	    $date['time']['hour'] = $test['hours'];
	    return $date;
    }    
    
    /**
     * List all PsCustomNoteCustomers.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pscustomnotecustomers.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pscustomnotecustomers to return.")
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    /*public function getAllAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $this->forwardIfNotAuthenticated();
        return $this->container->get('api.pscustomnotecustomer.handler')->all();
    }

    /**
     * Get single pscustomnotecustomer.
     *
     * @param int     $id      the pscustomnotecustomer id
     *
     * @Route(requirements={"_format"="json", "id": "\d+"}, defaults={"_format" = "json"})
     * @return array
     *
     * @throws NotFoundHttpException when pscustomnotecustomer not exist
     */
    public function getAction($id)
    {
        $this->forwardIfNotAuthenticated();
        $pscustomnotecustomer = $this->getOr404($id);
        return $pscustomnotecustomer;
    }

    /**
     * Presents the form to use to create a new pscustomnotecustomer.
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="html"}, defaults={"_format" = "html"})
     * @return FormTypeInterface
     */
    public function newAction()
    {
        $this->forwardIfNotAuthenticated();
        return $this->createForm(new PsCustomNoteCustomerType());
    }

    /**
     * Create an pscustomnotecustomer from the submitted data.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::newPsCustomNoteCustomer.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postAction(Request $request)
    {
        $this->forwardIfNotAuthenticated();
        $params = $request->request->all();
        try {
	        $params['apibundle_pscustomnotecustomer']['dateAdd'] = $this->now();
            $new = $this->container->get('api.pscustomnotecustomer.handler')->post(
                $params
            );

            return new JsonResponse($new, Codes::HTTP_CREATED);
            $routeOptions = array(
                'id' => $new->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('api_pscustomnotecustomer_get', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update existing pscustomnotecustomer from the submitted data or create a new pscustomnotecustomer at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsCustomNoteCustomer.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the pscustomnotecustomer id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when pscustomnotecustomer not exist
     */
    public function putAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $pscustomnotecustomer = $this->getOr404($id);
		try {
			if (!($pscustomnotecustomer = $this->container->get('api.pscustomnotecustomer.handler')->get($id))) {
				$statusCode = Codes::HTTP_CREATED;
				$pscustomnotecustomer = $this->container->get('api.pscustomnotecustomer.handler')->post(
					$request->request->all()
				);
			} else {
				$statusCode = Codes::HTTP_NO_CONTENT;
				$pscustomnotecustomer = $this->container->get('api.pscustomnotecustomer.handler')->put(
					$pscustomnotecustomer,
					$request->request->all()
				);
			}

			$routeOptions = array(
				'id' => $pscustomnotecustomer->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_pscustomnotecustomer_get', $routeOptions, $statusCode);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
    }

    /**
     * Update existing pscustomnotecustomer from the submitted data or create a new pscustomnotecustomer at a specific location.
     *
     *
     * @Annotations\View(
     *  template = "ApiBundle::editPsCustomNoteCustomer.html.twig",
     *  templateVar = "form"
     * )
     * @Route(requirements={"id": "\d+"})
     *
     * @param Request $request the request object
     * @param int     $id      the pscustomnotecustomer id
     *
     * @return FormTypeInterface|View
     *
     * @throws NotFoundHttpException when pscustomnotecustomer not exist
     */
    public function patchAction(Request $request, $id)
    {
        $this->forwardIfNotAuthenticated();
        $pscustomnotecustomer = $this->getOr404($id);
		try {
			$pscustomnotecustomer = $this->container->get('api.pscustomnotecustomer.handler')->patch(
				$this->getOr404($id),
				$request->request->all()
			);
			return $pscustomnotecustomer;

			$routeOptions = array(
				'id' => $pscustomnotecustomer->getId(),
				'_format' => $request->get('_format')
			);

			return $this->routeRedirectView('api_pscustomnotecustomer_get', $routeOptions, Codes::HTTP_NO_CONTENT);

		} catch (InvalidFormException $exception) {

			return $exception->getForm();
		}
    }

    /**
     * Fetch a PsCustomNoteCustomer or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return PsCustomNoteCustomer
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($pscustomnotecustomer = $this->container->get('api.pscustomnotecustomer.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        return $pscustomnotecustomer;
    }

    /**
	 * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
	 */
    public function deleteAction($id) {
        $this->forwardIfNotAuthenticated();
	    return $this->container->get('api.pscustomnotecustomer.handler')->delete($client);
    }
    
    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message='warn.user.notAuthenticated'){
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }
    
    /**
     * List all PsCustomNoteCustomers for the selected hive.
     *
     *
     * @Route(requirements={"_format"="json"}, defaults={"_format" = "json"})
     * @Annotations\QueryParam(name="idHive", requirements="\d+", nullable=false, description="Identifier of the hive for which get the already added custom notes.")
     *
     * @param integer               $idHive      identifier of a hive
     *
     * @return array
     */
    public function getHiveAction($id) {
        $this->forwardIfNotAuthenticated();
        return $this->getDoctrine()->getManager()->getRepository('ApiBundle:PsCustomNoteCustomer')->findByIdHive($id);
    }
}
?>
