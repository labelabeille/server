<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsCustomNoteCustomer
 *
 * @ORM\Table(name="ps_custom_notes_customer", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class PsCustomNoteCustomer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_custom_note", type="integer")
     */
    private $idCustomNote;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_hive", type="integer")
     */
    private $idHive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false, options={"default" = "now"})
     */
    private $dateAdd;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Set id_custom_note
     *
     * @param string $id_custom_note
     *
     * @return PsCustomNoteCustomer
     */
    public function setIdCustomNote($id_custom_note)
    {
        $this->idCustomNote = $id_custom_note;

        return $this;
    }

    /**
     * Get id_custom_note
     *
     * @return integer
     */
    public function getIdCustomNote()
    {
        return $this->idCustomNote;
    }

	/**
     * Set id_hive
     *
     * @param string $id_hive
     *
     * @return PsCustomNoteCustomer
     */
    public function setIdHive($id_hive)
    {
        $this->idHive = $id_hive;

        return $this;
    }

    /**
     * Get id_hive
     *
     * @return integer
     */
    public function getIdHive()
    {
        return $this->idHive;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsClient
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
}
