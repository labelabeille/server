<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsDefaultNoteCustomer
 *
 * @ORM\Table(name="ps_default_notes_customer", indexes={@ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class PsDefaultNoteCustomer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_default_note", type="integer")
     */
    private $idDefaultNote;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_hive", type="integer")
     */
    private $idHive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false, options={"default" = "now"})
     */
    private $dateAdd;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_customer", type="integer")
     */
    private $idCustomer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Set idDefaultNote
     *
     * @param string $idDefaultNote
     *
     * @return PsDefaultNoteCustomer
     */
    public function setIdDefaultNote($idDefaultNote)
    {
        $this->idDefaultNote = $idDefaultNote;

        return $this;
    }

    /**
     * Get idDefaultNote
     *
     * @return integer
     */
    public function getIdDefaultNote()
    {
        return $this->idDefaultNote;
    }

	/**
     * Set idHive
     *
     * @param string $idHive
     *
     * @return PsDefaultNoteCustomer
     */
    public function setIdHive($idHive)
    {
        $this->idHive = $idHive;

        return $this;
    }

    /**
     * Get idHive
     *
     * @return integer
     */
    public function getIdHive()
    {
        return $this->idHive;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsClient
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
    
    /**
     * Set idCustomer
     *
     * @param string $idCustomer
     *
     * @return PsDefaultNoteCustomer
     */
    public function setIdCustomer($idCustomer)
    {
        $this->idCustomer = $idCustomer;

        return $this;
    }

    /**
     * Get idCustomer
     *
     * @return integer
     */
    public function getIdCustomer()
    {
        return $this->idCustomer;
    }
}
