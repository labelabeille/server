<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsBox
 *
 * @ORM\Table(name="ps_box", indexes={@ORM\Index(name="id_hive", columns={"id_hive"}), @ORM\Index(name="id_client", columns={"id_client"}), @ORM\Index(name="serial_number", columns={"serial_number"}), @ORM\Index(name="version", columns={"version"}), @ORM\Index(name="active", columns={"active"}), @ORM\Index(name="date_add", columns={"date_add"}), @ORM\Index(name="date_upd", columns={"date_upd"})})
 * @ORM\Entity
 */
class PsBox
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_box", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBox;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_hive", type="integer", nullable=true)
     */
    private $idHive;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_client", type="integer", nullable=true)
     */
    private $idClient;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=32, nullable=false)
     */
    private $serialNumber = '';

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=32, nullable=false)
     */
    private $version = '';

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false, options={"default" = "now"})
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false, options={"default" = "now"})
     */
    private $dateUpd;

    
    /**
     * Get idBox
     *
     * @return integer
     */
    public function getId()
    {
        return $this->idBox;
    }

    /**
     * Get idBox
     *
     * @return integer
     */
    public function getIdBox()
    {
        return $this->idBox;
    }

    /**
     * Set idHive
     *
     * @param integer $idHive
     *
     * @return PsBox
     */
    public function setIdHive($idHive)
    {
        $this->idHive = $idHive;

        return $this;
    }

    /**
     * Get idHive
     *
     * @return integer
     */
    public function getIdHive()
    {
        return $this->idHive;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return PsBox
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return integer
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return PsBox
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return PsBox
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return PsBox
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsBox
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsBox
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsBox
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }
}
