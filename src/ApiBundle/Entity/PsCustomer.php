<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * PsCustomer
 *
 * @ORM\Table(name="ps_customer", indexes={@ORM\Index(name="customer_email", columns={"email"}), @ORM\Index(name="customer_login", columns={"email", "passwd"}), @ORM\Index(name="id_customer_passwd", columns={"id_customer", "passwd"}), @ORM\Index(name="id_gender", columns={"id_gender"}), @ORM\Index(name="id_shop_group", columns={"id_shop_group"}), @ORM\Index(name="id_shop", columns={"id_shop"})})
 * @ORM\Entity(repositoryClass="ApiBundle\Entity\CustomerRepository")
 */
class PsCustomer implements UserInterface, \Serializable
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id_customer", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop_group", type="integer", nullable=false)
     */
    private $idShopGroup = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_shop", type="integer", nullable=false)
     */
    private $idShop = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_gender", type="integer", nullable=false)
     */
    private $idGender = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_default_group", type="integer", nullable=false)
     */
    private $idDefaultGroup = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_lang", type="integer", nullable=true)
     */
    private $idLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_risk", type="integer", nullable=false)
     */
    private $idRisk = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=64, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=14, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="ape", type="string", length=5, nullable=true)
     */
    private $ape;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=32, nullable=false)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=32, nullable=false)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="passwd", type="string", length=32, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_passwd_gen", type="datetime", nullable=false, options={"default" = "now"})
     */
    private $lastPasswdGen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var boolean
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $newsletter;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_registration_newsletter", type="string", length=15, nullable=true)
     */
    private $ipRegistrationNewsletter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="newsletter_date_add", type="datetime", nullable=true)
     */
    private $newsletterDateAdd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="optin", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $optin;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=128, nullable=true)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="outstanding_allow_amount", type="decimal", precision=20, scale=6, nullable=false)
     */
    private $outstandingAllowAmount = '0.000000';

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_public_prices", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $showPublicPrices;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_payment_days", type="integer", nullable=false)
     */
    private $maxPaymentDays = '60';

    /**
     * @var string
     *
     * @ORM\Column(name="secure_key", type="string", length=32, nullable=false)
     */
    private $secureKey = '-1';

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $active;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_guest", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $isGuest;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false, options={"default" = "now"})
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;

    /**
     * @var integer
     *
     * @ORM\Column(name="hives_number", type="integer", nullable=false)
     */
    private $hivesNumber = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="hives_lost_number", type="integer", nullable=false)
     */
    private $hivesLostNumber = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="harvest", type="integer", nullable=false)
     */
    private $harvest = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="start_year", type="integer", nullable=false)
     */
    private $startYear = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="departament", type="string", length=64, nullable=false)
     */
    private $departament = '';

    /**
     * @var string
     *
     * @ORM\Column(name="registration_number", type="string", length=64, nullable=false)
     */
    private $registrationNumber = '';

    /**
     * @var string
     *
     * @ORM\Column(name="transhumance", type="string", length=64, nullable=false)
     */
    private $transhumance = '';

    /**
     * @var string
     *
     * @ORM\Column(name="structure", type="string", length=64, nullable=false)
     */
    private $structure = '';

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=64, nullable=false)
     */
    private $telephone = '';



    /**
     * Get idCustomer
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idShopGroup
     *
     * @param integer $idShopGroup
     *
     * @return PsCustomer
     */
    public function setIdShopGroup($idShopGroup)
    {
        $this->idShopGroup = $idShopGroup;

        return $this;
    }

    /**
     * Get idShopGroup
     *
     * @return integer
     */
    public function getIdShopGroup()
    {
        return $this->idShopGroup;
    }

    /**
     * Set idShop
     *
     * @param integer $idShop
     *
     * @return PsCustomer
     */
    public function setIdShop($idShop)
    {
        $this->idShop = $idShop;

        return $this;
    }

    /**
     * Get idShop
     *
     * @return integer
     */
    public function getIdShop()
    {
        return $this->idShop;
    }

    /**
     * Set idGender
     *
     * @param integer $idGender
     *
     * @return PsCustomer
     */
    public function setIdGender($idGender)
    {
        $this->idGender = $idGender;

        return $this;
    }

    /**
     * Get idGender
     *
     * @return integer
     */
    public function getIdGender()
    {
        return $this->idGender;
    }

    /**
     * Set idDefaultGroup
     *
     * @param integer $idDefaultGroup
     *
     * @return PsCustomer
     */
    public function setIdDefaultGroup($idDefaultGroup)
    {
        $this->idDefaultGroup = $idDefaultGroup;

        return $this;
    }

    /**
     * Get idDefaultGroup
     *
     * @return integer
     */
    public function getIdDefaultGroup()
    {
        return $this->idDefaultGroup;
    }

    /**
     * Set idLang
     *
     * @param integer $idLang
     *
     * @return PsCustomer
     */
    public function setIdLang($idLang)
    {
        $this->idLang = $idLang;

        return $this;
    }

    /**
     * Get idLang
     *
     * @return integer
     */
    public function getIdLang()
    {
        return $this->idLang;
    }

    /**
     * Set idRisk
     *
     * @param integer $idRisk
     *
     * @return PsCustomer
     */
    public function setIdRisk($idRisk)
    {
        $this->idRisk = $idRisk;

        return $this;
    }

    /**
     * Get idRisk
     *
     * @return integer
     */
    public function getIdRisk()
    {
        return $this->idRisk;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return PsCustomer
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return PsCustomer
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set ape
     *
     * @param string $ape
     *
     * @return PsCustomer
     */
    public function setApe($ape)
    {
        $this->ape = $ape;

        return $this;
    }

    /**
     * Get ape
     *
     * @return string
     */
    public function getApe()
    {
        return $this->ape;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return PsCustomer
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return PsCustomer
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return PsCustomer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set passwd
     *
     * @param string $passwd
     *
     * @return PsCustomer
     */
    public function setPassword($passwd)
    {
        $this->password = $passwd;

        return $this;
    }

    /**
     * Get passwd
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set lastPasswdGen
     *
     * @param \DateTime $lastPasswdGen
     *
     * @return PsCustomer
     */
    public function setLastPasswdGen($lastPasswdGen)
    {
        $this->lastPasswdGen = $lastPasswdGen;

        return $this;
    }

    /**
     * Get lastPasswdGen
     *
     * @return \DateTime
     */
    public function getLastPasswdGen()
    {
        return $this->lastPasswdGen;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return PsCustomer
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     *
     * @return PsCustomer
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return boolean
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set ipRegistrationNewsletter
     *
     * @param string $ipRegistrationNewsletter
     *
     * @return PsCustomer
     */
    public function setIpRegistrationNewsletter($ipRegistrationNewsletter)
    {
        $this->ipRegistrationNewsletter = $ipRegistrationNewsletter;

        return $this;
    }

    /**
     * Get ipRegistrationNewsletter
     *
     * @return string
     */
    public function getIpRegistrationNewsletter()
    {
        return $this->ipRegistrationNewsletter;
    }

    /**
     * Set newsletterDateAdd
     *
     * @param \DateTime $newsletterDateAdd
     *
     * @return PsCustomer
     */
    public function setNewsletterDateAdd($newsletterDateAdd)
    {
        $this->newsletterDateAdd = $newsletterDateAdd;

        return $this;
    }

    /**
     * Get newsletterDateAdd
     *
     * @return \DateTime
     */
    public function getNewsletterDateAdd()
    {
        return $this->newsletterDateAdd;
    }

    /**
     * Set optin
     *
     * @param boolean $optin
     *
     * @return PsCustomer
     */
    public function setOptin($optin)
    {
        $this->optin = $optin;

        return $this;
    }

    /**
     * Get optin
     *
     * @return boolean
     */
    public function getOptin()
    {
        return $this->optin;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return PsCustomer
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set outstandingAllowAmount
     *
     * @param string $outstandingAllowAmount
     *
     * @return PsCustomer
     */
    public function setOutstandingAllowAmount($outstandingAllowAmount)
    {
        $this->outstandingAllowAmount = $outstandingAllowAmount;

        return $this;
    }

    /**
     * Get outstandingAllowAmount
     *
     * @return string
     */
    public function getOutstandingAllowAmount()
    {
        return $this->outstandingAllowAmount;
    }

    /**
     * Set showPublicPrices
     *
     * @param boolean $showPublicPrices
     *
     * @return PsCustomer
     */
    public function setShowPublicPrices($showPublicPrices)
    {
        $this->showPublicPrices = $showPublicPrices;

        return $this;
    }

    /**
     * Get showPublicPrices
     *
     * @return boolean
     */
    public function getShowPublicPrices()
    {
        return $this->showPublicPrices;
    }

    /**
     * Set maxPaymentDays
     *
     * @param integer $maxPaymentDays
     *
     * @return PsCustomer
     */
    public function setMaxPaymentDays($maxPaymentDays)
    {
        $this->maxPaymentDays = $maxPaymentDays;

        return $this;
    }

    /**
     * Get maxPaymentDays
     *
     * @return integer
     */
    public function getMaxPaymentDays()
    {
        return $this->maxPaymentDays;
    }

    /**
     * Set secureKey
     *
     * @param string $secureKey
     *
     * @return PsCustomer
     */
    public function setSecureKey($secureKey)
    {
        $this->secureKey = $secureKey;

        return $this;
    }

    /**
     * Get secureKey
     *
     * @return string
     */
    public function getSecureKey()
    {
        return $this->secureKey;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return PsCustomer
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsCustomer
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set isGuest
     *
     * @param boolean $isGuest
     *
     * @return PsCustomer
     */
    public function setIsGuest($isGuest)
    {
        $this->isGuest = $isGuest;

        return $this;
    }

    /**
     * Get isGuest
     *
     * @return boolean
     */
    public function getIsGuest()
    {
        return $this->isGuest;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return PsCustomer
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsCustomer
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsCustomer
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }

    /**
     * Set hivesNumber
     *
     * @param integer $hivesNumber
     *
     * @return PsCustomer
     */
    public function setHivesNumber($hivesNumber)
    {
        $this->hivesNumber = $hivesNumber;

        return $this;
    }

    /**
     * Get hivesNumber
     *
     * @return integer
     */
    public function getHivesNumber()
    {
        return $this->hivesNumber;
    }

    /**
     * Set hivesLostNumber
     *
     * @param integer $hivesLostNumber
     *
     * @return PsCustomer
     */
    public function setHivesLostNumber($hivesLostNumber)
    {
        $this->hivesLostNumber = $hivesLostNumber;

        return $this;
    }

    /**
     * Get hivesLostNumber
     *
     * @return integer
     */
    public function getHivesLostNumber()
    {
        return $this->hivesLostNumber;
    }

    /**
     * Set harvest
     *
     * @param integer $harvest
     *
     * @return PsCustomer
     */
    public function setHarvest($harvest)
    {
        $this->harvest = $harvest;

        return $this;
    }

    /**
     * Get harvest
     *
     * @return integer
     */
    public function getHarvest()
    {
        return $this->harvest;
    }

    /**
     * Set startYear
     *
     * @param integer $startYear
     *
     * @return PsCustomer
     */
    public function setStartYear($startYear)
    {
        $this->startYear = $startYear;

        return $this;
    }

    /**
     * Get startYear
     *
     * @return integer
     */
    public function getStartYear()
    {
        return $this->startYear;
    }

    /**
     * Set departament
     *
     * @param string $departament
     *
     * @return PsCustomer
     */
    public function setDepartament($departament)
    {
        $this->departament = $departament;

        return $this;
    }

    /**
     * Get departament
     *
     * @return string
     */
    public function getDepartament()
    {
        return $this->departament;
    }

    /**
     * Set registrationNumber
     *
     * @param string $registrationNumber
     *
     * @return PsCustomer
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set transhumance
     *
     * @param string $transhumance
     *
     * @return PsCustomer
     */
    public function setTranshumance($transhumance)
    {
        $this->transhumance = $transhumance;

        return $this;
    }

    /**
     * Get transhumance
     *
     * @return string
     */
    public function getTranshumance()
    {
        return $this->transhumance;
    }

    /**
     * Set structure
     *
     * @param string $structure
     *
     * @return PsCustomer
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;

        return $this;
    }

    /**
     * Get structure
     *
     * @return string
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return PsCustomer
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }


    /**
     * For authentication only
     *
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt() {
        return 'Vd6GRfQ4vJ3s0dSmP4EURM17CZwhXypqsAeq6x39I248UmNhG4c6RxrP';
    }

    public function getRoles() {
        return ['ROLE_USER'];
    }

    public function eraseCredentials() {}

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
}
