<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsHive
 *
 * @ORM\Table(name="ps_hive", indexes={@ORM\Index(name="id_client", columns={"id_client"}), @ORM\Index(name="latitude", columns={"latitude"}), @ORM\Index(name="longitude", columns={"longitude"}), @ORM\Index(name="active", columns={"active"}), @ORM\Index(name="date_add", columns={"date_add"}), @ORM\Index(name="date_upd", columns={"date_upd"})})
 * @ORM\Entity
 */
class PsHive
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_hive", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idHive;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_client", type="integer", nullable=false)
     */
    private $idClient = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_hive_group", type="integer", nullable=false)
     */
    private $idHiveGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=13, scale=8, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=13, scale=8, nullable=true)
     */
    private $longitude;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default" = "0"})
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="hive_type", type="string", length=64, nullable=true)
     */
    private $hiveType;

    /**
     * @var string
     *
     * @ORM\Column(name="bees_type", type="string", length=64, nullable=true)
     */
    private $beesType;

    /**
     * @var string
     *
     * @ORM\Column(name="material", type="string", length=64, nullable=true)
     */
    private $material;

    /**
     * @var string
     *
     * @ORM\Column(name="support", type="string", length=64, nullable=true)
     */
    private $support;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=64, nullable=true)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="harvest", type="integer", nullable=false)
     */
    private $harvest = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;

    /**
     * Get idHive
     *
     * @return integer
     */
    public function getIdHive()
    {
        return $this->idHive;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return PsHive
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return integer
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set idHiveGroup
     *
     * @param integer $idHiveGroup
     *
     * @return PsHive
     */
    public function setIdHiveGroup($idHiveGroup)
    {
        $this->idHiveGroup = $idHiveGroup;

        return $this;
    }

    /**
     * Get idHiveGroup
     *
     * @return integer
     */
    public function getIdHiveGroup()
    {
        return $this->idHiveGroup;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsHive
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return PsHive
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return PsHive
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return PsHive
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PsHive
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set hiveType
     *
     * @param string $hiveType
     *
     * @return PsHive
     */
    public function setHiveType($hiveType)
    {
        $this->hiveType = $hiveType;

        return $this;
    }

    /**
     * Get hiveType
     *
     * @return string
     */
    public function getHiveType()
    {
        return $this->hiveType;
    }

    /**
     * Set beesType
     *
     * @param string $beesType
     *
     * @return PsHive
     */
    public function setBeesType($beesType)
    {
        $this->beesType = $beesType;

        return $this;
    }

    /**
     * Get beesType
     *
     * @return string
     */
    public function getBeesType()
    {
        return $this->beesType;
    }

    /**
     * Set material
     *
     * @param string $material
     *
     * @return PsHive
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set support
     *
     * @param string $support
     *
     * @return PsHive
     */
    public function setSupport($support)
    {
        $this->support = $support;

        return $this;
    }

    /**
     * Get support
     *
     * @return string
     */
    public function getSupport()
    {
        return $this->support;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return PsHive
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set harvest
     *
     * @param integer $harvest
     *
     * @return PsHive
     */
    public function setHarvest($harvest)
    {
        $this->harvest = $harvest;

        return $this;
    }

    /**
     * Get harvest
     *
     * @return integer
     */
    public function getHarvest()
    {
        return $this->harvest;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return PsHive
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsHive
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsHive
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }

    public function getCoordinates() {
        $hive = ['idruche'=>$this->getIdHive(), 'lat'=>$this->getLatitude(), 'lng'=>$this->getLongitude()];
        return $hive;
    }
}
