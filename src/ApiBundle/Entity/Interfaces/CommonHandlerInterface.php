<?php
namespace ApiBundle\Entity\Interfaces;

/**
 * An interface that all the objects should implement.
 * In most circumstances, only a single object should implement
 * this interface as the ResolveTargetEntityListener can only
 * change the target to a single object.
 */
interface CommonHandlerInterface
{
    // List any additional methods that your UserBundle
    // will need to access on the subject so that you can
    // be sure that you have access to those methods.

    /**
     * @return string
     */
    public function get($id);
    
    /**
     * @return string
     */
    public function post(array $p);
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters);
    
    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters);
}
?>
