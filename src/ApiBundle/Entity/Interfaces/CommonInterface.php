<?php
namespace ApiBundle\Entity\Interfaces;

/**
 * An interface that all the other objects should implement.
 * In most circumstances, only a single object should implement
 * this interface as the ResolveTargetEntityListener can only
 * change the target to a single object.
 */
interface CommonInterface
{
    // List any additional methods that your ApiBundle
    // will need to access on the subject so that you can
    // be sure that you have access to those methods.
}
?>
