<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsHiveGroup
 *
 * @ORM\Table(name="ps_hive_group", indexes={@ORM\Index(name="id_client", columns={"id_client"}), @ORM\Index(name="date_add", columns={"date_add"}), @ORM\Index(name="date_upd", columns={"date_upd"})})
 * @ORM\Entity
 */
class PsHiveGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_hive_group", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idHiveGroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_client", type="integer", nullable=false)
     */
    private $idClient = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="harvest", type="integer", nullable=false)
     */
    private $harvest = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_upd", type="datetime", nullable=false)
     */
    private $dateUpd;


    /**
     * Get idHiveGroup
     *
     * @return integer
     */
    public function getId() {
        return $this->idHiveGroup;
    }

    /**
     * Get idHiveGroup
     *
     * @return integer
     */
    public function getIdHiveGroup()
    {
        return $this->idHiveGroup;
    }

    /**
     * Set idClient
     *
     * @param integer $idClient
     *
     * @return PsHive
     */
    public function setIdClient($idClient)
    {
        $this->idClient = $idClient;

        return $this;
    }

    /**
     * Get idClient
     *
     * @return integer
     */
    public function getIdClient()
    {
        return $this->idClient;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsHive
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return PsHive
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set harvest
     *
     * @param integer $harvest
     *
     * @return PsHive
     */
    public function setHarvest($harvest)
    {
        $this->harvest = $harvest;

        return $this;
    }

    /**
     * Get harvest
     *
     * @return integer
     */
    public function getHarvest()
    {
        return $this->harvest;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return PsHive
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return PsHive
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpd
     *
     * @param \DateTime $dateUpd
     *
     * @return PsHive
     */
    public function setDateUpd($dateUpd)
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    /**
     * Get dateUpd
     *
     * @return \DateTime
     */
    public function getDateUpd()
    {
        return $this->dateUpd;
    }
    
    function get_json() {
        $json = array();
        foreach($this as $key => $value) {
            $json[$key] = $value;
        }
        return $json;
    }
}
