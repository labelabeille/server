<?php
namespace ApiBundle\Entity\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use ApiBundle\Entity\Interfaces\CommonHandlerInterface;
use ApiBundle\Entity\Interfaces\CommonInterface;
use ApiBundle\Form\PsCustomNoteCustomerType;
use ApiBundle\Entity\PsCustomNoteCustomer;
use ApiBundle\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

class PsCustomNoteCustomerHandler implements CommonHandlerInterface {

    private $formFactory;
    private $om;
    private $entityClass;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    public function get($id) {
        return $this->repository->find($id);
    }


    /**
     * Get a list of PsCustomNoteCustomers.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }


    /**
     * Create a new PsCustomNoteCustomer.
     *
     * @param array $parameters
     *
     * @return PsCustomNoteCustomerInterface
     */
    public function post(array $parameters) {
        $pscustomnotecustomer = $this->createPsCustomNoteCustomer(); // factory method create an empty PsCustomNoteCustomer

        // Process form does all the magic, validate and hydrate the PsCustomNoteCustomer Object.
        return $this->processForm($pscustomnotecustomer, $parameters, 'POST');
    }

    /**
     * Processes the form.
     *
     * @param PsCustomNoteCustomerInterface $pscustomnotecustomer
     * @param array         $parameters
     * @param String        $method
     *
     * @return PsCustomNoteCustomerInterface
     *
     * @throws \Acme\BlogBundle\Exception\InvalidFormException
     */
    private function processForm(PsCustomNoteCustomer $pscustomnotecustomer, array $parameters, $method = "PUT") {
        $parameters = $parameters['apibundle_pscustomnotecustomer'];
        $form = $this->formFactory->create(new PsCustomNoteCustomerType(), $pscustomnotecustomer, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $pscustomnotecustomer = $form->getData();
            $this->om->persist($pscustomnotecustomer);
            $this->om->flush($pscustomnotecustomer);

            return $pscustomnotecustomer;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    /**
     * Presents the form to use to create a new pscustomnotecustomer.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @View()
     *
     * @return FormTypeInterface
     */
    public function newAction() {
        return $this->createForm(new PsCustomNoteCustomerType());
    }

    private function createPsCustomNoteCustomer()
    {
        return new $this->entityClass();
    }
    
    public function delete(PsCustomNoteCustomer $pscustomnotecustomer) {
        $this->om->remove($pscustomnotecustomer);
        $this->om->flush(); 
        $response = new JsonResponse(['success' => true], 301);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PATCH');
    }
}
