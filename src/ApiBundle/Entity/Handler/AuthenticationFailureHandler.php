<?php

namespace ApiBundle\Entity\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

/**
 * class AuthenticationFailureHandler
 *
 */
class AuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure( Request $request, AuthenticationException $exception )
    {
        $response = new JsonResponse(['success' => false, 'exception' => json_encode($exception->getMessage()), 'request' => json_encode($request->request->all())], 401);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
?>
