<?php
namespace ApiBundle\Entity\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use ApiBundle\Entity\Interfaces\CommonHandlerInterface;
use ApiBundle\Entity\Interfaces\CommonInterface;
use ApiBundle\Form\PsDefaultNoteType;
use ApiBundle\Entity\PsDefaultNote;
use ApiBundle\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

class PsDefaultNoteHandler implements CommonHandlerInterface {

    private $formFactory;
    private $om;
    private $entityClass;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    public function get($id) {
        return $this->repository->find($id);
    }


    /**
     * Get a list of PsDefaultNotes.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }


    /**
     * Create a new PsDefaultNote.
     *
     * @param array $parameters
     *
     * @return PsDefaultNoteInterface
     */
    public function post(array $parameters) {
        $psdefaultnote = $this->createPsDefaultNote(); // factory method create an empty PsDefaultNote

        // Process form does all the magic, validate and hydrate the PsDefaultNote Object.
        return $this->processForm($psdefaultnote, $parameters, 'POST');
    }

    /**
     * Processes the form.
     *
     * @param PsDefaultNoteInterface $psdefaultnote
     * @param array         $parameters
     * @param String        $method
     *
     * @return PsDefaultNoteInterface
     *
     * @throws \Acme\BlogBundle\Exception\InvalidFormException
     */
    private function processForm(PsDefaultNote $psdefaultnote, array $parameters, $method = "PUT") {
        $parameters = $parameters['apibundle_psdefaultnote'];
        $form = $this->formFactory->create(new PsDefaultNoteType(), $psdefaultnote, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $psdefaultnote = $form->getData();
            $this->om->persist($psdefaultnote);
            $this->om->flush($psdefaultnote);

            return $psdefaultnote;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    /**
     * Presents the form to use to create a new psdefaultnote.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @View()
     *
     * @return FormTypeInterface
     */
    public function newAction() {
        return $this->createForm(new PsDefaultNoteType());
    }

    private function createPsDefaultNote()
    {
        return new $this->entityClass();
    }
    
    public function delete(PsDefaultNote $psdefaultnote) {
        $this->om->remove($psdefaultnote);
        $this->om->flush(); 
        $response = new JsonResponse(['success' => true], 301);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PATCH');
    }
}
