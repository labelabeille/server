<?php
namespace ApiBundle\Entity\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use ApiBundle\Entity\Interfaces\CommonHandlerInterface;
use ApiBundle\Entity\Interfaces\CommonInterface;
use ApiBundle\Form\PsCustomNoteType;
use ApiBundle\Entity\PsCustomNote;
use ApiBundle\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

class PsCustomNoteHandler implements CommonHandlerInterface {

    private $formFactory;
    private $om;
    private $entityClass;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    public function get($id) {
        return $this->repository->find($id);
    }


    /**
     * Get a list of PsCustomNotes.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = null, $offset = null)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }


    /**
     * Create a new PsCustomNote.
     *
     * @param array $parameters
     *
     * @return PsCustomNoteInterface
     */
    public function post(array $parameters) {
        $pscustomnote = $this->createPsCustomNote(); // factory method create an empty PsCustomNote

        // Process form does all the magic, validate and hydrate the PsCustomNote Object.
        return $this->processForm($pscustomnote, $parameters, 'POST');
    }

    /**
     * Processes the form.
     *
     * @param PsCustomNoteInterface $pscustomnote
     * @param array         $parameters
     * @param String        $method
     *
     * @return PsCustomNoteInterface
     *
     * @throws \Acme\BlogBundle\Exception\InvalidFormException
     */
    private function processForm(PsCustomNote $pscustomnote, array $parameters, $method = "PUT") {
        $parameters = $parameters['apibundle_pscustomnote'];
        $form = $this->formFactory->create(new PsCustomNoteType(), $pscustomnote, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $pscustomnote = $form->getData();
            $this->om->persist($pscustomnote);
            $this->om->flush($pscustomnote);

            return $pscustomnote;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    /**
     * Presents the form to use to create a new pscustomnote.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @View()
     *
     * @return FormTypeInterface
     */
    public function newAction() {
        return $this->createForm(new PsCustomNoteType());
    }

    private function createPsCustomNote()
    {
        return new $this->entityClass();
    }
    
    public function delete(PsCustomNote $pscustomnote) {
        $this->om->remove($pscustomnote);
        $this->om->flush(); 
        $response = new JsonResponse(['success' => true], 301);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PATCH');
    }
}
