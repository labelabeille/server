<?php

namespace ApiBundle\Entity\Handler;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * class AuthenticationSuccessHandler
 *
 * @author Nicolas Macherey <nicolas.macherey@gmail.com>
 */
class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess( Request $request, TokenInterface $token )
    {
        $response = new JsonResponse(['success' => true, 
            'email' => $token->getUser()->getUsername(), 
            'id' => $token->getUser()->getId(),
            'firstname' =>$token->getUser()->getFirstname(),
            'lastname' =>$token->getUser()->getLastname(),], 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
        // on effectue la redirection
        /*$response = new RedirectResponse($this->router->generate('api_pscustomer_me'));
        $event->setResponse($response);
        //$response->headers->set('Access-Control-Allow-Origin', "http://localhost");
        return $response;*/
    }
}
?>
