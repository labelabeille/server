<?php
namespace ApiBundle\Entity\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use ApiBundle\Entity\Interfaces\CommonHandlerInterface;
use ApiBundle\Entity\Interfaces\CommonInterface;
use ApiBundle\Entity\PsCustomer;
use ApiBundle\Form\PsCustomerType;
use ApiBundle\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

class PsCustomerHandler implements CommonHandlerInterface {

    private $formFactory;
    private $om;
    private $entityClass;
    private $repository;
    
    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }
    
    public function get($id) {
        return $this->repository->find($id);
    }
    
    /**
     * Get a list of PsCustomers.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }

    
    /**
     * Create a new PsCustomer.
     *
     * @param array $parameters
     *
     * @return PsCustomerInterface
     */
    public function post(array $parameters) {
        $pscustomer = $this->createPsCustomer(); // factory method create an empty PsCustomer

        // Process form does all the magic, validate and hydrate the PsCustomer Object.
        return $this->processForm($pscustomer, $parameters, 'POST');
    }
    
    
    /**
     * Processes the form.
     *
     * @param PsCustomerInterface $pscustomer
     * @param array         $parameters
     * @param String        $method
     *
     * @return PsCustomerInterface
     *
     * @throws \Acme\BlogBundle\Exception\InvalidFormException
     */
    private function processForm(PsCustomer $pscustomer, array $parameters, $method = "PUT") {
        $parameters = $parameters['apibundle_pscustomer'];
        $form = $this->formFactory->create(new PsCustomerType(), $pscustomer, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $pscustomer = $form->getData();
            if((isset($parameters['active']))&&($parameters['active'] == 0)) {
                $pscustomer->setActive(false);
            }
            
            $this->om->persist($pscustomer);
            $this->om->flush($pscustomer);

            return $pscustomer;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }
    
    /**
     * Presents the form to use to create a new pscustomer.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @View()
     *
     * @return FormTypeInterface
     */
    public function newAction() {
        return $this->createForm(new PsCustomerType());
    }
    
    private function createPsCustomer()
    {
        return new $this->entityClass();
    }
    
    public function delete(PsCustomer $pshive) {
        $this->om->remove($pshive);
        $this->om->flush();
        $response = new JsonResponse(['success' => true], 301);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PATCH');
    }
}
