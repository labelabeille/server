<?php
namespace ApiBundle\Entity\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use ApiBundle\Entity\Interfaces\CommonHandlerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use ApiBundle\Entity\Interfaces\CommonInterface;
use ApiBundle\Form\PsHiveGroupType;
use ApiBundle\Entity\PsHiveGroup;
use ApiBundle\Exception\InvalidFormException;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

class PsHiveGroupHandler implements CommonHandlerInterface {

    private $formFactory;
    private $om;
    private $entityClass;
    private $repository;
    
    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }
    
    public function get($id) {
        return $this->repository->find($id);
    }
    
    /**
     * Get a list of PsHiveGroups.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }

    
    /**
     * Create a new PsHiveGroup.
     *
     * @param array $parameters
     *
     * @return PsHiveGroupInterface
     */
    public function post(array $parameters) {
        $pshivegroup = $this->createPsHiveGroup(); // factory method create an empty PsHiveGroup

        // Process form does all the magic, validate and hydrate the PsHiveGroup Object.
        return $this->processForm($pshivegroup, $parameters, 'POST');
    }
    
    
    /**
     * Processes the form.
     *
     * @param PsHiveGroupInterface $pshivegroup
     * @param array         $parameters
     * @param String        $method
     *
     * @return PsHiveGroupInterface
     *
     * @throws \Acme\BlogBundle\Exception\InvalidFormException
     */
    private function processForm(PsHiveGroup $pshivegroup, array $parameters, $method = "PUT") {
        $parameters = $parameters['apibundle_pshivegroup'];
        $form = $this->formFactory->create(new PsHiveGroupType(), $pshivegroup, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $pshivegroup = $form->getData();
            $this->om->persist($pshivegroup);
            $this->om->flush($pshivegroup);

            return $pshivegroup;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }
    
    /**
     * Presents the form to use to create a new pshivegroup.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @View()
     *
     * @return FormTypeInterface
     */
    public function newAction() {
        return $this->createForm(new PsHiveGroupType());
    }
    
    private function createPsHiveGroup()
    {
        return new $this->entityClass();
    }

    public function delete(PsHiveGroup $pshivegroup) {
        $this->om->remove($pshivegroup);
        $this->om->flush(); 
        $response = new JsonResponse(['success' => true],301);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PATCH');
    }
}
?>
