<?php
namespace ApiBundle\Entity\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use ApiBundle\Entity\Interfaces\CommonHandlerInterface;
use ApiBundle\Entity\Interfaces\CommonInterface;
use ApiBundle\Form\PsBoxType;
use ApiBundle\Entity\PsBox;
use ApiBundle\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Debug\Exception\ClassNotFoundException;

class PsBoxHandler implements CommonHandlerInterface {

    private $formFactory;
    private $om;
    private $entityClass;
    private $repository;
    
    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }
    
    public function get($id) {
        return $this->repository->find($id);
    }
    
    /**
     * Get a list of PsBoxs.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }

    
    /**
     * Create a new PsBox.
     *
     * @param array $parameters
     *
     * @return PsBoxInterface
     */
    public function post(array $parameters) {
        $psbox = $this->createPsBox(); // factory method create an empty PsBox

        // Process form does all the magic, validate and hydrate the PsBox Object.
        return $this->processForm($psbox, $parameters, 'POST');
    }
    
    
    /**
     * Processes the form.
     *
     * @param PsBoxInterface $psbox
     * @param array         $parameters
     * @param String        $method
     *
     * @return PsBoxInterface
     *
     * @throws \Acme\BlogBundle\Exception\InvalidFormException
     */
    private function processForm(PsBox $psbox, array $parameters, $method = "PUT") {
        $parameters = $parameters['apibundle_psbox'];
        $form = $this->formFactory->create(new PsBoxType(), $psbox, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $psbox = $form->getData();
            $this->om->persist($psbox);
            $this->om->flush($psbox);

            return $psbox;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }
    
    /**
     * Presents the form to use to create a new psbox.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @View()
     *
     * @return FormTypeInterface
     */
    public function newAction() {
        return $this->createForm(new PsBoxType());
    }
    
    private function createPsBox()
    {
        return new $this->entityClass();
    }
    
    public function delete(PsBox $psbox) {
        $this->om->remove($psbox);
        $this->om->flush(); 
        $response = new JsonResponse(['success' => true], 301);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * Edit a Page, or create if not exist.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function put($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param PageInterface $page
     * @param array         $parameters
     *
     * @return PageInterface
     */
    public function patch($page, array $parameters)
    {
        return $this->processForm($page, $parameters, 'PATCH');
    }
}
